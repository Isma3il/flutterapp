import 'package:data/config/config.dart';
import 'package:domain/entities/video.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class VideoItem extends StatelessWidget {
  final Video video;

  VideoItem(this.video);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Card(
        elevation: 8,
        margin: EdgeInsets.all(5.0),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0))),
        child: Stack(
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              child: Image.network(
                  "${Config.YOUTUBE_THUMBMAIL}${video.key}/0.jpg",
                  fit: BoxFit.cover),
            ),
            Positioned(top:100,bottom: 100,left: 100,right: 100,child: Image.asset("images/logotype.png",width: 20,height: 20,))
          ],
        ),
      ),
      onTap: (){

      },
    );
  }
}
