import 'dart:io';

import 'package:core/platform/network_info.dart';
import 'package:data/api/api_helper.dart';
import 'package:data/config/config.dart';
import 'package:data/data.dart';
import 'package:data/repositories/movie_favorite_repository_impl.dart';
import 'package:data/repositories/movie_repository_impl.dart';
import 'package:data/repositories/movie_details_repository_impl.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:dio/dio.dart';
import 'package:domain/repositories/movie_detail_repository.dart';
import 'package:domain/repositories/movie_favorite_repository.dart';
import 'package:domain/repositories/movie_repository.dart';
import 'package:domain/usecases/favorite_use_case.dart';
import 'package:domain/usecases/most_popular_use_case.dart';
import 'package:domain/usecases/movie_details_use_case.dart';
import 'package:domain/usecases/top_rated_use_case.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get_it/get_it.dart';
import 'package:moviecttd/bloc/favorite/favorite_bloc.dart';
import 'package:moviecttd/bloc/most_popular/bloc.dart';
import 'package:moviecttd/bloc/movie_details/movie_details_bloc.dart';
import 'package:moviecttd/bloc/top_rated/bloc.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

final locator = GetIt.instance;

Future<void> init() async {
  /*Bloc*/

  locator.registerFactory(() => TopRatedBloc(useCase: locator()));
  locator.registerFactory(() => MostPopularBloc(useCase: locator()));
  locator.registerFactory(() => MovieDetailsBloc(useCase: locator()));
  locator.registerFactory(() => FavoriteBloc(useCase: locator()));


  /*UseCases*/

  locator.registerLazySingleton<TopRatedUseCase>(() => TopRatedUseCaseImpl(locator()));
  locator.registerLazySingleton<MostPopularUseCase>(() => MostPopularUseCaseImpl(locator()));
  locator.registerLazySingleton<MovieDetailUseCase>(() => MovieDetailsUseCaseImpl(locator()));
  locator.registerLazySingleton<FavoriteUseCase>(() => FavoriteUseCaseImpl(locator()));

  /*Repository*/
  locator.registerLazySingleton<MovieRepository>(() => MovieRepositoryImpl(
      apiHelper: locator(),
      dbHelper: locator(),
      prefHelper: locator(),
      networkInfo: locator()));

  locator.registerLazySingleton<MovieDetailRepository>(() =>
      MovieDetailsRepositoryImpl(apiHelper: locator(), networkInfo: locator(),dbHelper: locator()));

  locator.registerLazySingleton<MovieFavoriteRepository>(() => MovieFavoriteRepositoryImpl(dbHelper: locator()));


  /*Data*/
  locator.registerLazySingleton<ApiHelper>(() => AppApiHelper(dio: locator()));
  locator.registerLazySingleton<DbHelper>(() => AppDbHelper(database: locator()));
  locator.registerLazySingleton<PrefHelper>(
      () => AppPrefHelper(secureStorage: locator()));

  /*Core*/
  locator.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl(locator()));

  /*External*/
  locator.registerLazySingleton(() => DataConnectionChecker());
  locator.registerLazySingleton(() => FlutterSecureStorage());

  //Dio

  locator.registerLazySingleton<Dio>(() {
    BaseOptions options = new BaseOptions(
      baseUrl: "${Config.BASE_URL}",
      connectTimeout: 5000,
      receiveTimeout: 3000,
    );
    Dio dio = Dio(options);
    int _maxCharactersPerLine = 200;

    dio.interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions options) async {
      // Do something before request is sent
      // If you want to resolve the request with some custom data，
      // you can return a `Response` object or return `dio.resolve(data)`.
      // If you want to reject the request with a error message,
      // you can return a `DioError` object or return `dio.reject(errMsg)`
      print("--> ${options.method} ${options.path}");
      print("Content type: ${options.contentType}");
      print("<-- END HTTP");
      return options;
    }, onResponse: (Response response) async {
      // Do something with response data
      print(
          "<-- ${response.statusCode} ${response.request.method} ${response.request.path}");
      String responseAsString = response.data.toString();
      if (responseAsString.length > _maxCharactersPerLine) {
        int iterations =
            (responseAsString.length / _maxCharactersPerLine).floor();
        for (int i = 0; i <= iterations; i++) {
          int endingIndex = i * _maxCharactersPerLine + _maxCharactersPerLine;
          if (endingIndex > responseAsString.length) {
            endingIndex = responseAsString.length;
          }
          print(responseAsString.substring(
              i * _maxCharactersPerLine, endingIndex));
        }
      } else {
        print(response.data);
      }
      print("<-- END HTTP");
      return response; // continue
    }, onError: (DioError err) async {
      // Do something with response error
      print("<-- Error -->");
      print(err.error);
      print(err.message);
      return err; //continue
    }));

    return dio;
  });




  locator.registerLazySingleton<Future<Database>>(() async {

    var dbMovie = await openDatabase(
        "movie.db", version: 1, onCreate: (Database db, int newVersion) async{
      await db.execute(
          "CREATE TABLE movie(id INTEGER PRIMARY KEY, title TEXT, " +
              "description TEXT, release_data TEXT, vote_count INTEGER,"
                  " vote_average DOUBLE, poster_path TEXT)");
    });


    return   dbMovie;
  });
}
