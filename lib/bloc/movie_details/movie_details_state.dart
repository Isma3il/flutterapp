import 'package:domain/entities/review.dart';
import 'package:domain/entities/video.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class MovieDetailsState extends Equatable {
   MovieDetailsState([List props = const <dynamic>[]]):super(props);
}

class InitialMovieDetailsState extends MovieDetailsState {}

class LoadingDetails extends MovieDetailsState{}


/* Movie Reviews */
class ReviewsLoadedSuccess extends MovieDetailsState{
   final List<Review> reviewList;

   ReviewsLoadedSuccess({@required this.reviewList}):super([reviewList]);

}
class ReviewsLoadedFailed extends MovieDetailsState{
   final String errorMessage;
   ReviewsLoadedFailed({@required this.errorMessage}):super([errorMessage]);
}


/*  Movie Videos  */
class VideosLoadedSuccess extends MovieDetailsState{

   final List<Video> videoList;

   VideosLoadedSuccess({@required this.videoList}):super([videoList]);
}
class VideosLoadedFailed extends MovieDetailsState{
   final String errorMessage;
   VideosLoadedFailed({@required this.errorMessage}):super([errorMessage]);
}


/* IS Movie Favorite */
class MovieFromFavorite extends MovieDetailsState{}
class MovieNotFromFavorite extends MovieDetailsState{}
class MovieCacheError extends MovieDetailsState{
  final String errorMessage;
   MovieCacheError({@required this.errorMessage}):super([errorMessage]);
}

