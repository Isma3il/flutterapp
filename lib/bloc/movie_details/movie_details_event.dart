import 'package:domain/entities/movie.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class MovieDetailsEvent extends Equatable {
   MovieDetailsEvent([List props = const <dynamic>[]]):super(props);
}

class FetchReviews extends MovieDetailsEvent{
   final String movieId;

  FetchReviews({@required this.movieId}):super([movieId]);

}
class FetchVideos extends MovieDetailsEvent{
   final String movieId;

   FetchVideos({@required this.movieId}):super([movieId]);
}

class IsMovieAddedToFavorite extends MovieDetailsEvent{
  final String movieId;
  IsMovieAddedToFavorite({@required this.movieId}):super([movieId]);
}


class AddMovieToFavorite extends MovieDetailsEvent{
  final Movie movie;
  AddMovieToFavorite({@required this.movie}):super([movie]);
}

class RemoveMovieFromFavorite extends MovieDetailsEvent{
  final int id;

  RemoveMovieFromFavorite({@required this.id}):super([id]);
}