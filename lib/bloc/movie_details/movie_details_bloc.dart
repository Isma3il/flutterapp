import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:core/error/failure.dart';
import 'package:domain/usecases/movie_details_use_case.dart';
import 'package:flutter/material.dart';
import './bloc.dart';
import 'package:meta/meta.dart';

class MovieDetailsBloc extends Bloc<MovieDetailsEvent, MovieDetailsState> {
  final MovieDetailUseCase useCase;

  MovieDetailsBloc({@required this.useCase}) : assert(useCase != null);

  @override
  MovieDetailsState get initialState => InitialMovieDetailsState();

  @override
  Stream<MovieDetailsState> mapEventToState(MovieDetailsEvent event,) async* {

    /*Reviews*/
    if (event is FetchReviews) {
      yield LoadingDetails();
      final result = await useCase.getMovieReviews(event.movieId);
      yield* result.fold((failure) async* {
        yield ReviewsLoadedFailed(errorMessage: _mapFailureToMessage(failure));
      }, (result) async* {
        yield ReviewsLoadedSuccess(reviewList: result);
      });
    }
    /*Videos*/
    if (event is FetchVideos) {
      yield LoadingDetails();
      final result = await useCase.getMovieVideos(event.movieId);
      yield* result.fold((failure) async* {
        yield VideosLoadedFailed(errorMessage: _mapFailureToMessage(failure));
      }, (result) async* {
        yield VideosLoadedSuccess(videoList: result);
      });
    }

    /*Favorite*/
    if (event is IsMovieAddedToFavorite) {
      final result = await useCase.getMovie(int.parse(event.movieId));

      yield* result.fold((failure) async* {
        yield MovieCacheError(errorMessage: _mapFailureToMessage(failure));
      }, (result) async* {
        if (result != null) {
          yield MovieFromFavorite();
        } else {
          yield MovieNotFromFavorite();
        }
      });
    }

    if (event is AddMovieToFavorite) {
      final result = await useCase.insertMovie(event.movie);
      yield* result.fold((failure) async* {
        yield MovieCacheError(errorMessage: _mapFailureToMessage(failure));
      }, (result) async* {
        yield MovieFromFavorite();
      });
    }

    if (event is RemoveMovieFromFavorite) {
      final result = await useCase.deleteMovie(event.id);
      yield* result.fold((failure) async* {
        yield MovieCacheError(errorMessage: _mapFailureToMessage(failure));
      }, (result) async* {
        yield MovieNotFromFavorite();
      });
    }
  }


}




String _mapFailureToMessage(Failure failure) {
  // Instead of a regular 'if (failure is ServerFailure)...'
  switch (failure.runtimeType) {
    case ServerFailure:
      return "Error on fetching data from server";
    case CacheFailure:
      return "Error on fetching data from storage";
    default:
      return 'Unexpected Error';
  }
}
