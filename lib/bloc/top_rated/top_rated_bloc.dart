import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:core/error/failure.dart';
import 'package:domain/repositories/movie_repository.dart';
import 'package:domain/usecases/top_rated_use_case.dart';
import './bloc.dart';
import 'package:meta/meta.dart';

class TopRatedBloc extends Bloc<TopRatedEvent, TopRatedState> {
  final TopRatedUseCase useCase;

  TopRatedBloc({@required this.useCase})
      : assert(useCase != null);

  @override
  TopRatedState get initialState => InitialTopRatedState();

  @override
  Stream<TopRatedState> mapEventToState(TopRatedEvent event,) async* {
    if(event is Fetch){
      yield LoadingTopRatedState();

       final result=  await useCase.getTopRatedMovies();

      yield* result.fold (
               (failure) async*{yield LoadedFailedTopRatedState(errorMsg: _mapFailureToMessage(failure));},

               (result)  async* {yield  LoadedSuccessTopRatedState(movieList: result);}
       );


    }
  }

  String _mapFailureToMessage(Failure failure) {
    // Instead of a regular 'if (failure is ServerFailure)...'
    switch (failure.runtimeType) {
      case ServerFailure:
        return "Error on fetching data from server";
      case CacheFailure:
        return "Error on fetching data from storage";
      default:
        return 'Unexpected Error';
    }
  }

}
