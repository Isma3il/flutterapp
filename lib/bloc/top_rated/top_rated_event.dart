import 'package:equatable/equatable.dart';

abstract class TopRatedEvent extends Equatable {
   TopRatedEvent([List props = const <dynamic>[]]):super(props);
}

class Fetch extends TopRatedEvent{}