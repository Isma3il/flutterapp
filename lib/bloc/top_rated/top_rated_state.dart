import 'package:domain/entities/movie.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';


abstract class TopRatedState extends Equatable {
   TopRatedState([List props = const <dynamic>[]]):super(props);
}

class InitialTopRatedState extends TopRatedState {}

class LoadingTopRatedState extends TopRatedState{}

class LoadedSuccessTopRatedState extends TopRatedState{
  final List<Movie> movieList;

  LoadedSuccessTopRatedState({@required this.movieList}):super([movieList]);
}

class LoadedFailedTopRatedState extends TopRatedState{
  final String errorMsg;
  LoadedFailedTopRatedState({@required this.errorMsg}):super([errorMsg]);
}