import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:core/error/failure.dart';
import 'package:domain/repositories/movie_favorite_repository.dart';
import 'package:domain/usecases/favorite_use_case.dart';
import './bloc.dart';
import 'package:meta/meta.dart';

class FavoriteBloc extends Bloc<FavoriteEvent, FavoriteState> {
  final FavoriteUseCase useCase;

  FavoriteBloc({@required this.useCase}) : assert(useCase != null);

  @override
  FavoriteState get initialState => EmptyFavoriteMovies();

  @override
  Stream<FavoriteState> mapEventToState(
    FavoriteEvent event,
  ) async* {


    if (event is FetchFavoriteMovies) {
      yield LoadingFavoriteMovies();

      final result = await useCase.getMovies();

      yield* result.fold((failure) async* {
        yield LoadedFailedFavoriteMovies(_mapFailureToMessage(failure));
      }, (result) async* {
        yield LoadedSuccessFavoriteMovies(result);
      });


    } else if (event is DeleteFavoriteMovie) {

      final result = await useCase.deleteMovie(int.parse(event.movieId));

      yield* result.fold((failure) async* {
        yield LoadedFailedFavoriteMovies(_mapFailureToMessage(failure));
      }, (result) async* {
        final result = await useCase.getMovies();
        yield* result.fold((failure) async* {
          yield LoadedFailedFavoriteMovies(_mapFailureToMessage(failure));
        }, (result) async* {
          yield LoadedSuccessFavoriteMovies(result);
        });
      });


    }
  }
}

String _mapFailureToMessage(Failure failure) {
  // Instead of a regular 'if (failure is ServerFailure)...'
  switch (failure.runtimeType) {
    case ServerFailure:
      return "Error on fetching data from server";
    case CacheFailure:
      return "Error on fetching data from storage";
    default:
      return 'Unexpected Error';
  }
}
