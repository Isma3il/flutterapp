import 'package:domain/entities/movie.dart';
import 'package:equatable/equatable.dart';

abstract class FavoriteState extends Equatable {
   FavoriteState([List props = const <dynamic>[]]):super(props);
}

class EmptyFavoriteMovies extends FavoriteState {}
class LoadingFavoriteMovies extends FavoriteState{}


class LoadedSuccessFavoriteMovies extends FavoriteState{
   final List<Movie> movieList;

   LoadedSuccessFavoriteMovies(this.movieList):super([movieList]);
}
class LoadedEmptyFavoriteMovies extends FavoriteState{}
class DeleteFavoriteMovies extends FavoriteState{}



class LoadedFailedFavoriteMovies extends FavoriteState{
   final String errorMessage;

   LoadedFailedFavoriteMovies(this.errorMessage):super([errorMessage]);
}

