import 'package:equatable/equatable.dart';

abstract class FavoriteEvent extends Equatable {
   FavoriteEvent([List props = const <dynamic>[]]):super(props);
}

class FetchFavoriteMovies extends FavoriteEvent{}

class DeleteFavoriteMovie extends FavoriteEvent{
  final String movieId;
  DeleteFavoriteMovie(this.movieId):super([movieId]);
}