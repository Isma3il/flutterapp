import 'package:domain/entities/movie.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class MostPopularState extends Equatable {
   MostPopularState([List props = const <dynamic>[]]):super(props);
}

class InitialMostPopularState extends MostPopularState {
  @override
  List<Object> get props => [];
}


class Loading extends MostPopularState{}

class LoadedSuccess extends MostPopularState{
  final List<Movie> movieList;

  LoadedSuccess({@required this.movieList}):super([movieList]);

}

class LoadedFailed extends MostPopularState{
  final String errMsg;

  LoadedFailed({@required this.errMsg}):super([errMsg]);

}