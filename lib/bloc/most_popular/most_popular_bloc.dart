import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:core/error/failure.dart';
import 'package:domain/usecases/most_popular_use_case.dart';
import 'package:meta/meta.dart';
import 'package:domain/repositories/movie_repository.dart';
import './bloc.dart';

class MostPopularBloc extends Bloc<MostPopularEvent, MostPopularState> {

  final MostPopularUseCase useCase;

  MostPopularBloc({@required this.useCase}) :assert(useCase!=null);

  @override
  MostPopularState get initialState => InitialMostPopularState();

  @override
  Stream<MostPopularState> mapEventToState(
    MostPopularEvent event,
  ) async* {
    if(event is Fetch){
      yield Loading();
      final result=  await useCase.getMostPopularMovie();

      yield* result.fold (
              (failure) async*{yield LoadedFailed(errMsg: _mapFailureToMessage(failure));},

              (result)  async* {yield  LoadedSuccess(movieList: result);}
      );
    }
  }
}


String _mapFailureToMessage(Failure failure) {
  // Instead of a regular 'if (failure is ServerFailure)...'
  switch (failure.runtimeType) {
    case ServerFailure:
      return "Error on fetching data from server";
    case CacheFailure:
      return "Error on fetching data from storage";
    default:
      return 'Unexpected Error';
  }
}