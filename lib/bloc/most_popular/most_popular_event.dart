import 'package:equatable/equatable.dart';

abstract class MostPopularEvent extends Equatable {
   MostPopularEvent([List props = const <dynamic>[]]):super(props);
}
class Fetch extends MostPopularEvent{}