import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:moviecttd/bloc/most_popular/bloc.dart';
import 'package:moviecttd/widgets/loading.dart';
import 'file:///D:/Flutter/movie_cttd/lib/widgets/items/movie_item.dart';

class MostPopular extends StatefulWidget {
  @override
  _MostPopularState createState() => _MostPopularState();
}

class _MostPopularState extends State<MostPopular> {
  @override
  Widget build(BuildContext context) {
    BlocProvider.of<MostPopularBloc>(context)
      ..add(Fetch());

    return BlocBuilder<MostPopularBloc, MostPopularState>(
      // ignore: missing_return
        builder: (context, state) {
          if (state is Loading) {
            return LoadingMovies();
          } else if (state is LoadedSuccess) {
            return Container(
              child: GridView.count(
                crossAxisCount: 2,
                padding: EdgeInsets.all(4),
                childAspectRatio: 1.0 / 1.0,
                children: state.movieList.map((e) => MovieItem(e)).toList(),
              ),
            );
          }else if(state is LoadedFailed){
            return Center(child: Text(state.errMsg),);
          }else{
            return Container();
          }
        });
  }
}
