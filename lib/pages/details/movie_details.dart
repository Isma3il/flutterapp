import 'package:domain/entities/movie.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:moviecttd/bloc/movie_details/movie_details_bloc.dart';
import 'package:moviecttd/pages/details/info.dart';
import 'package:moviecttd/pages/details/movie_details_controller.dart';
import 'package:moviecttd/pages/details/reviews.dart';
import 'package:moviecttd/pages/details/videos.dart';
import 'package:moviecttd/service_locator.dart';

class MovieDetails extends StatefulWidget {
  final Movie movie;

  MovieDetails({@required this.movie}) : assert(movie != null);

  @override
  _MovieDetailsState createState() => _MovieDetailsState(movie: movie);
}

class _MovieDetailsState extends State<MovieDetails>
    with SingleTickerProviderStateMixin {
  _MovieDetailsState({this.movie});

  Movie movie;
  bool isFav;
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: 3);
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (_) => locator<MovieDetailsBloc>(),
        child: Scaffold(
            body: DefaultTabController(
                length: 3,
                child: NestedScrollView(
                  headerSliverBuilder:
                      (BuildContext context, bool innerBoxIsScrolled) {
                    return <Widget>[
                      SliverAppBar(
                        expandedHeight: 300.0,
                        floating: false,
                        pinned: true,
                        flexibleSpace: FlexibleSpaceBar(
                            centerTitle: true,
                            title: Text("${movie.title}",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16.0,
                                )),
                            background: Image.network(
                              "${movie.poster}",
                              fit: BoxFit.cover,
                            )),
                      ),
                      SliverPersistentHeader(
                        delegate: _SliverAppBarDelegate(
                          TabBar(
                            controller: _tabController,
                            labelColor: Colors.green,
                            unselectedLabelColor: Colors.grey,
                            tabs: [
                              Tab(icon: Icon(Icons.info), text: "Info"),
                              Tab(
                                  icon: Icon(Icons.lightbulb_outline),
                                  text: "Reviews"),
                              Tab(
                                  icon: Icon(Icons.ondemand_video),
                                  text: "Videos"),
                            ],
                          ),
                        ),
                        pinned: true,
                      ),
                    ];
                  },
                  body: Center(
                    child: TabBarView(
                      children: [
                        Info(
                          movie: movie,
                        ),
                        Review(
                          movie: movie,
                        ),
                        Videos(
                          movie: movie,
                        )
                      ],
                      controller: _tabController,
                    ),
                  ),
                )),
          floatingActionButton: MovieDetailsController(movie: movie),
    ));
  }

}

class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  _SliverAppBarDelegate(this._tabBar);

  final TabBar _tabBar;

  @override
  double get minExtent => _tabBar.preferredSize.height;

  @override
  double get maxExtent => _tabBar.preferredSize.height;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return new Container(
      color: Colors.white,
      child: _tabBar,
    );
  }

  @override
  bool shouldRebuild(_SliverAppBarDelegate oldDelegate) {
    return false;
  }
}
