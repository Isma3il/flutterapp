import 'package:domain/entities/movie.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:moviecttd/bloc/movie_details/movie_details_bloc.dart';
import 'package:moviecttd/bloc/movie_details/movie_details_event.dart';
import 'package:moviecttd/bloc/movie_details/movie_details_state.dart';
import 'package:moviecttd/widgets/items/review_item.dart';
import 'package:moviecttd/widgets/loading.dart';


class Review extends StatefulWidget {
  final Movie movie;

  Review({this.movie});
  @override
  _ReviewState createState() => _ReviewState(movie: movie);
}

class _ReviewState extends State<Review> {
  final Movie movie;

  _ReviewState({this.movie});
  @override
  Widget build(BuildContext context) {
    BlocProvider.of<MovieDetailsBloc>(context)
    ..add(FetchReviews(movieId: movie.id.toString()));


    return BlocBuilder<MovieDetailsBloc,MovieDetailsState>(
      // ignore: missing_return
      builder: (context,state){
        if(state is LoadingDetails){
          return LoadingMovies();
        }else if(state is ReviewsLoadedSuccess){
          return ListView.builder(itemBuilder: (context,position){
            return ReviewItem(state.reviewList[position]);
          },padding: EdgeInsets.all(10),shrinkWrap: true,itemCount: state.reviewList.length,);
        }else if(state is ReviewsLoadedFailed){
          return Center(child: Text(state.errorMessage),);
        }else{
          return Container();
        }
      },
    );
  }
}
