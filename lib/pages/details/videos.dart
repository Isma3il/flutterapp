import 'package:domain/entities/movie.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:moviecttd/bloc/movie_details/movie_details_bloc.dart';
import 'package:moviecttd/bloc/movie_details/movie_details_event.dart';
import 'package:moviecttd/bloc/movie_details/movie_details_state.dart';
import 'package:moviecttd/widgets/items/video_item.dart';
import 'package:moviecttd/widgets/loading.dart';

class Videos extends StatefulWidget {
  final Movie movie;

  Videos({this.movie});

  @override
  _VideosState createState() => _VideosState(movie: movie);
}

class _VideosState extends State<Videos> {
  final Movie movie;

  _VideosState({this.movie});

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<MovieDetailsBloc>(context)..add(FetchVideos(movieId: movie.id.toString()));

    return BlocBuilder<MovieDetailsBloc, MovieDetailsState>(
        // ignore: missing_return
        builder: (context,state){
          if(state is LoadingDetails){
            return LoadingMovies();
          }else if(state is VideosLoadedSuccess){
            return ListView.builder(itemBuilder: (context,position){
              return VideoItem(state.videoList[position]);
            },padding: EdgeInsets.all(10),shrinkWrap: true,itemCount: state.videoList.length,);
          }else if(state is VideosLoadedFailed){
            return Center(child: Text(state.errorMessage),);
          }else{
            return Container();
          }
        },

    );
  }
}
