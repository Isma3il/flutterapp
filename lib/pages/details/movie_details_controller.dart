import 'package:domain/entities/movie.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:moviecttd/bloc/movie_details/movie_details_bloc.dart';
import 'package:moviecttd/bloc/movie_details/movie_details_event.dart';
import 'package:moviecttd/bloc/movie_details/movie_details_state.dart';
class MovieDetailsController extends StatefulWidget {

  final Movie movie;
  MovieDetailsController({@required this.movie}):assert(movie!=null);

  @override
  _MovieDetailsControllerState createState() => _MovieDetailsControllerState(movie);
}

class _MovieDetailsControllerState extends State<MovieDetailsController> {

  final Movie movie;
  _MovieDetailsControllerState(this.movie);


  bool isFav;

  @override
  void initState() {
    super.initState();
    BlocProvider.of<MovieDetailsBloc>(context)
        .add(IsMovieAddedToFavorite(movieId: movie.id.toString()));

  }

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      child: BlocBuilder<MovieDetailsBloc,MovieDetailsState>(
        builder: (context,state){
          if(state is MovieFromFavorite){
            isFav=true;
            return Icon(
              Icons.favorite,
              color: Colors.yellow,
            );
          }else if(state is MovieNotFromFavorite){
            isFav=false;
            return Icon(
              Icons.favorite_border,
              color: Colors.white,
            );
          }else{
            isFav=false;
            return Icon(
              Icons.favorite_border,
              color: Colors.white,
            );
          }
        },
      ), onPressed: () {
      if(isFav){
        removeMovieFromFavorite();
      }else{
        addMovieToFavorite();
      }
    }
    );
  }
  void removeMovieFromFavorite() {
    BlocProvider.of<MovieDetailsBloc>(context)
        .add(RemoveMovieFromFavorite(id: movie.id));
  }

  void addMovieToFavorite() {
    BlocProvider.of<MovieDetailsBloc>(context)
        .add(AddMovieToFavorite(movie: movie));
  }
}

