import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:moviecttd/bloc/top_rated/bloc.dart';
import 'package:moviecttd/widgets/items/movie_item.dart';
import 'package:moviecttd/widgets/loading.dart';

class TopRatedMovies extends StatefulWidget {
  @override
  _TopRatedMoviesState createState() => _TopRatedMoviesState();
}

class _TopRatedMoviesState extends State<TopRatedMovies> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<TopRatedBloc>(context)
      ..add(Fetch());

    return BlocBuilder<TopRatedBloc, TopRatedState>(
      // ignore: missing_return
        builder: (context, state) {
          if (state is LoadingTopRatedState) {
            return LoadingMovies();
          } else if (state is LoadedSuccessTopRatedState) {
            return Container(
              child: GridView.count(
                crossAxisCount: 2,
                padding: EdgeInsets.all(4),
                childAspectRatio: 1.0 / 1.0,
                children: state.movieList.map((e) => MovieItem(e)).toList(),
              ),
            );
          }else if(state is LoadedFailedTopRatedState){
            return Center(child: Text(state.errorMsg),);
          }else{
            return Container();
          }
        });
  }
}
