import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:moviecttd/bloc/favorite/favorite_bloc.dart';
import 'package:moviecttd/bloc/top_rated/bloc.dart';
import 'package:moviecttd/pages/favorite.dart';
import 'package:moviecttd/pages/most_popular.dart';
import 'package:moviecttd/pages/top_rated.dart';
import 'package:moviecttd/bloc/most_popular/bloc.dart';
import 'package:moviecttd/service_locator.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _selectedPage = 0;
  final _pageOption = [
    TopRatedMovies(),
    MostPopular(),
    FavoriteMovies(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Movie App"),
      ),
      body: MultiBlocProvider(providers: [
        BlocProvider(
          create: (_) => locator<TopRatedBloc>(),
        ),
        BlocProvider(
          create: (_) => locator<MostPopularBloc>(),
        ),
        BlocProvider(
          create: (_) => locator<FavoriteBloc>(),
        ),

      ], child: _pageOption[_selectedPage]),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _selectedPage,
        onTap: (index) {
          setState(() {
            _selectedPage = index;
          });
        },
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.home), title: Text("Top Rated")),
          BottomNavigationBarItem(
              icon: Icon(Icons.autorenew), title: Text("Popular")),
          BottomNavigationBarItem(
              icon: Icon(Icons.history), title: Text("History")),
        ],
      ),
    );
  }
}
