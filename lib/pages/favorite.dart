import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:moviecttd/bloc/favorite/bloc.dart';
import 'package:moviecttd/widgets/items/fav_item.dart';
import 'package:moviecttd/widgets/loading.dart';

class FavoriteMovies extends StatefulWidget {
  @override
  _FavoriteMoviesState createState() => _FavoriteMoviesState();
}

class _FavoriteMoviesState extends State<FavoriteMovies> {
  @override
  Widget build(BuildContext context) {

    BlocProvider.of<FavoriteBloc>(context).add(FetchFavoriteMovies());

    return Scaffold(
      body: BlocBuilder<FavoriteBloc,FavoriteState>(builder: (context, state) {
        if (state is LoadingFavoriteMovies) {
          return LoadingMovies();
        } else if (state is LoadedSuccessFavoriteMovies) {
          return ListView.builder(
              itemBuilder: (context, position) {
                return Slidable(
                  direction: Axis.horizontal,
                  actionPane: SlidableDrawerActionPane(),
                  actionExtentRatio: 0.25,
                  child: FavItem(
                    movie: state.movieList[position],
                  ),
                  actions: <Widget>[
                    IconSlideAction(
                      caption: 'Delete',
                      color: Colors.red,
                      icon: Icons.delete,
                      onTap: () {
                        //removeMovieFromFavorite(position);
                      },
                    )
                  ],
                );
              },
              itemCount: state.movieList.length,
              padding: EdgeInsets.all(5.0));
        }else if(state is EmptyFavoriteMovies){
          return Container(
            child: Center(child: Text("you didn't have any favorite movie"),),
          );
        }else if(state is LoadedFailedFavoriteMovies){
          return Container(
            child: Center(child: Text("something error happened"),),
          );
        }else{
          return Container();
        }

      }),
    );
  }
}
