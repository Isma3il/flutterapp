

import 'package:dartz/dartz.dart';
import 'package:domain/repositories/movie_repository.dart';
import 'package:domain/usecases/most_popular_use_case.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../fake_data.dart';

class MockMovieRepository extends Mock implements MovieRepository{}


void main(){
  MostPopularUseCaseImpl popularUseCase;
  MockMovieRepository mockMovieRepository;


  setUp((){
    mockMovieRepository=MockMovieRepository();
    popularUseCase=MostPopularUseCaseImpl(mockMovieRepository);
  });

  test("test most popular use case", () async{

    /*arrange*/
    when(mockMovieRepository.getMostPopularMovie())
        .thenAnswer((_) async => Right(movieListTest));

    /*act*/
    final result = await popularUseCase.getMostPopularMovie();

    /*assert*/

    // UseCase should simply return whatever was returned from the Repository
    expect(result, Right(movieListTest));

    // Verify that the method has been called on the Repository
    verify(mockMovieRepository.getMostPopularMovie());

    // Only the above method should be called and nothing more.
    verifyNoMoreInteractions(mockMovieRepository);


  });


}
