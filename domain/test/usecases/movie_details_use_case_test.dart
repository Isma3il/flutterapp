
import 'package:dartz/dartz.dart';
import 'package:domain/repositories/movie_detail_repository.dart';
import 'package:domain/usecases/movie_details_use_case.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../fake_data.dart';

class MockMovieDetailsRepository extends Mock implements MovieDetailRepository{}




void main() {
  MockMovieDetailsRepository movieDetailsRepository;
  MovieDetailsUseCaseImpl movieDetailsUseCaseImpl;

  setUp((){
      movieDetailsRepository = MockMovieDetailsRepository();
      movieDetailsUseCaseImpl = MovieDetailsUseCaseImpl(movieDetailsRepository);
  });



  group("test movie details use case", (){
    test("test reviews", () async{

      //arrange
      when(movieDetailsRepository.getMovieReviews(""))
      .thenAnswer((_) async => Right(reviewListTest));


      //act
      final result= await movieDetailsUseCaseImpl.getMovieReviews("");

      //assert
      expect(result, Right(reviewListTest));
      verify(movieDetailsRepository.getMovieReviews(""));
      verifyNoMoreInteractions(movieDetailsRepository);

    });
    
    
    
    test("test videos", () async {

       //arrange
      when(movieDetailsRepository.getMovieVideos(""))
      .thenAnswer((_) async => Right(videoListTest));


      //act
      final result= await movieDetailsUseCaseImpl.getMovieVideos("");

      //assert
      expect(result, Right(videoListTest));
      verify(movieDetailsRepository.getMovieVideos(""));
      verifyNoMoreInteractions(movieDetailsRepository);


    });



  });

}