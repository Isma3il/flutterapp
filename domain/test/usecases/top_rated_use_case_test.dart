import 'package:core/error/exception.dart';
import 'package:core/error/failure.dart';
import 'package:dartz/dartz.dart';
import 'package:domain/repositories/movie_repository.dart';
import 'package:domain/usecases/top_rated_use_case.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../fake_data.dart';

class MockMovieRepository extends Mock implements MovieRepository{}


void main(){

  MockMovieRepository mockMovieRepository;
  TopRatedUseCaseImpl topRatedUseCaseImpl;


  setUp((){
    mockMovieRepository=MockMovieRepository();
    topRatedUseCaseImpl=TopRatedUseCaseImpl(mockMovieRepository);
  });


  group("test top rated use case", (){

    test("success case top rated use case",()async{

      /*arrange*/
      when(mockMovieRepository.getTopRatedMovie())
          .thenAnswer((_) async => Right(movieListTest));

      /*act*/
      final result = await topRatedUseCaseImpl.getTopRatedMovies();

      /*assert*/
      expect(result, Right(movieListTest));

      verify(mockMovieRepository.getTopRatedMovie());

      verifyNoMoreInteractions(mockMovieRepository);

    });



    /*Don't care :) it's not interested test*/    /*ملوش لازمه*/
    test("failed case top rated use case",()async{

      /*arrange*/
      when(mockMovieRepository.getTopRatedMovie())
          .thenAnswer((_) async => Left(ServerFailure()));

      /*act*/
      final result = await topRatedUseCaseImpl.getTopRatedMovies();

      /*assert*/
      expect(result, Left(ServerFailure()));

      verify(mockMovieRepository.getTopRatedMovie());

      verifyNoMoreInteractions(mockMovieRepository);

    });



  });





}