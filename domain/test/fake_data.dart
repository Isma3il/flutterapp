import 'package:domain/entities/movie.dart';
import 'package:domain/entities/review.dart';
import 'package:domain/entities/video.dart';

final List<Movie> movieListTest = [
  Movie(id: 1, title: "test1", description: "test movie 1", poster: "test poster 1", backdoorPoster: "test backdoor1", releaseData: "1-1-2001", voteAverage: 5.9, voteCount: 9),
  Movie(id: 2, title: "test2", description: "test movie 2", poster: "test poster 1", backdoorPoster: "test backdoor1", releaseData: "1-1-2001", voteAverage: 5.9, voteCount: 7),
  Movie(id: 3, title: "test3", description: "test movie 3", poster: "test poster 1", backdoorPoster: "test backdoor1", releaseData: "1-1-2001", voteAverage: 5.9, voteCount: 6),
  Movie(id: 4, title: "test4", description: "test movie 4", poster: "test poster 1", backdoorPoster: "test backdoor1", releaseData: "1-1-2001", voteAverage: 5.9, voteCount: 5),
  Movie(id: 5, title: "test5", description: "test movie 5", poster: "test poster 1", backdoorPoster: "test backdoor1", releaseData: "1-1-2001", voteAverage: 5.9, voteCount: 2),

];



final List<Review>  reviewListTest= [
  Review(author: "test1", content: "test1", id: "test1", url: "test1"),
  Review(author: "test2", content: "test2", id: "test2", url: "test2 "),
  Review(author: "test3", content: "test3", id: "test3", url: "test3"),
  Review(author: "test4", content: "test4", id: "test4", url: "test4"),
];


final List<Video> videoListTest=[
  Video(id: "test 1", key: "test 1"),
  Video(id: "test 2", key: "test 2"),
  Video(id: "test 3", key: "test 3"),
  Video(id: "test 4", key: "test 4"),
];