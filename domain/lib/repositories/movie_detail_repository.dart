import 'package:core/error/failure.dart';
import 'package:dartz/dartz.dart';
import 'package:domain/entities/movie.dart';
import 'package:domain/entities/review.dart';
import 'package:domain/entities/video.dart';

abstract class MovieDetailRepository {
  Future<Either<Failure, List<Review>>> getMovieReviews(String movieId);
  Future<Either<Failure, List<Video>>> getMovieVideos(String movieId);

  Future<Either<Failure,Movie>> getMovie(int id);
  Future<Either<Failure,int>> insertMovie(Movie movie);
  Future<Either<Failure,int>> deleteMovie(int id);


}
