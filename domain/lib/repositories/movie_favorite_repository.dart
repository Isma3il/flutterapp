
import 'package:core/error/failure.dart';
import 'package:dartz/dartz.dart';
import 'package:domain/entities/movie.dart';

abstract class MovieFavoriteRepository{


  Future<Either<Failure,List<Movie>>> getMovies();
  Future<Either<Failure,int>> deleteMovie(int id);

}