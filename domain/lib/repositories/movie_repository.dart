import 'package:core/error/failure.dart';
import 'package:dartz/dartz.dart';
import '../entities/movie.dart';


abstract class MovieRepository{

  Future<Either<Failure , List<Movie>>> getTopRatedMovie();
  Future<Either<Failure , List<Movie>>> getMostPopularMovie();

}