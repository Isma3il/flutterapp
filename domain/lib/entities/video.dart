import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class Video extends Equatable{
  final String id;
  final String key;

  Video({@required this.id,@required this.key}):super([id,key]);
}