import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
class Movie extends Equatable{
  final int id;
  final String title;
  final String description;
  final String poster;
  final String backdoorPoster;
  final String releaseData;
  final double voteAverage;
  final int voteCount;

  Movie({@required this.id,@required this.title,@required  this.description,@required  this.poster,@required  this.backdoorPoster,
    @required  this.releaseData,@required  this.voteAverage,@required  this.voteCount}):super([id,title,description,poster,backdoorPoster,releaseData,voteAverage,voteCount]);
}