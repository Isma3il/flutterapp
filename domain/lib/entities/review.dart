import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class Review extends Equatable{
  final String author;
  final String content;
  final String id;
  final String url;

  Review({@required this.author,@required  this.content,
    @required  this.id,@required  this.url}):super([author,content,id,url]);
}