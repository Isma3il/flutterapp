import 'package:core/error/failure.dart';
import 'package:dartz/dartz.dart';
import 'package:domain/entities/movie.dart';
import 'package:domain/repositories/movie_repository.dart';

abstract class MostPopularUseCase{
    Future<Either<Failure,List<Movie>>> getMostPopularMovie();

}

class MostPopularUseCaseImpl implements MostPopularUseCase{
  final MovieRepository repository;

  MostPopularUseCaseImpl(this.repository);

  @override
  Future<Either<Failure, List<Movie>>> getMostPopularMovie() async=> repository.getMostPopularMovie();
}