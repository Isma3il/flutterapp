import 'package:core/error/failure.dart';
import 'package:dartz/dartz.dart';
import 'package:domain/entities/movie.dart';
import 'package:domain/repositories/movie_repository.dart';

abstract class TopRatedUseCase{


  Future<Either<Failure,List<Movie>>> getTopRatedMovies();

}


class TopRatedUseCaseImpl implements TopRatedUseCase{

  final MovieRepository repository;

  TopRatedUseCaseImpl(this.repository);

  @override
  Future<Either<Failure, List<Movie>>> getTopRatedMovies() async => repository.getTopRatedMovie();

}