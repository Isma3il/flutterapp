
import 'package:core/error/failure.dart';
import 'package:dartz/dartz.dart';
import 'package:domain/entities/movie.dart';
import 'package:domain/repositories/movie_favorite_repository.dart';


abstract class FavoriteUseCase{
  Future<Either<Failure,List<Movie>>> getMovies();
  Future<Either<Failure,int>> deleteMovie(int id);
}

class FavoriteUseCaseImpl implements FavoriteUseCase{
  final MovieFavoriteRepository repository;

  FavoriteUseCaseImpl(this.repository);


  @override
  Future<Either<Failure, int>> deleteMovie(int id) => repository.deleteMovie(id);

  @override
  Future<Either<Failure, List<Movie>>> getMovies() => repository.getMovies();

}