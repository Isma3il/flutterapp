import 'package:core/error/failure.dart';
import 'package:dartz/dartz.dart';
import 'package:domain/entities/movie.dart';
import 'package:domain/entities/review.dart';
import 'package:domain/entities/video.dart';
import 'package:domain/repositories/movie_detail_repository.dart';

abstract class MovieDetailUseCase {
  Future<Either<Failure, List<Review>>> getMovieReviews(String movieId);
  Future<Either<Failure, List<Video>>> getMovieVideos(String movieId);

  Future<Either<Failure, Movie>> getMovie(int id);
  Future<Either<Failure, int>> insertMovie(Movie movie);
  Future<Either<Failure, int>> deleteMovie(int id);
}

class MovieDetailsUseCaseImpl implements MovieDetailUseCase {
  final MovieDetailRepository repository;

  MovieDetailsUseCaseImpl(this.repository);

  @override
  Future<Either<Failure, List<Review>>> getMovieReviews(String movieId) =>
      repository.getMovieReviews(movieId);
  @override
  Future<Either<Failure, List<Video>>> getMovieVideos(String movieId) =>
      repository.getMovieVideos(movieId);

  @override
  Future<Either<Failure, int>> deleteMovie(int id) => repository.deleteMovie(id);

  @override
  Future<Either<Failure, Movie>> getMovie(int id) => repository.getMovie(id);

  @override
  Future<Either<Failure, int>> insertMovie(Movie movie) => repository.insertMovie(movie);
}
