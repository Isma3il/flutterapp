import 'package:data/api/model/movie_response.dart';
import 'package:data/api/model/movie_model.dart';

final List<MovieModel> results = [
  MovieModel(
      backdrop_path: "test",
      adult: false,
      genre_ids: [80, 18],
      id: 278,
      original_language: "en",
      original_title: "test",
      overview:
          "test",
      popularity: 63.268,
      poster_path: "test",
      release_date: "1994-09-23",
      title: "test",
      video: false,
      vote_average: 8.9,
      vote_count: 16355)
];

final movieResponseTest =
    MoviesResponse(page: 1, total_pages: 1, total_results: 1, results: results);
