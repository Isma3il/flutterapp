import 'dart:convert';

import 'package:data/api/model/movie_response.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../fake_data.dart';
import '../../responses/response_reader.dart';

void main() {





  group("from json", () {

    test("return valid model", (){
      //arrange
      final Map<String,dynamic>jsonMap=json.decode(readResponse('movie_response.json'));

      //act
      final result = MoviesResponse.fromJson(jsonMap);


      expect(result.results, movieResponseTest.results);

    });

  });
}
