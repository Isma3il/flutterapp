
import 'package:data/api/model/movie_response.dart';
import 'package:data/api/model/movie_reviews.dart';
import 'package:data/api/model/movie_videos.dart';

abstract class ApiHelper{

  Future<MoviesResponse> getTopRatedMovie();

  Future<MoviesResponse> getMostPopularMovie();

  Future<MovieReviews> getMovieReviews(String movieId);

  Future<MovieVideos> getMovieVideos(String movieId);

}