import 'package:data/api/model/review_model.dart';

class MovieReviews {
  final int id;
  final int page;
  final List<ReviewModel> results;
  final int total_pages;
  final int total_results;

  MovieReviews({this.id, this.page, this.results, this.total_pages, this.total_results});

  factory MovieReviews.fromJson(Map<String, dynamic> json) {
    return MovieReviews(
      id: json['id'],
      page: json['page'],
      results: json['results'] != null ? (json['results'] as List).map((i) => ReviewModel.fromJson(i)).toList() : null,
      total_pages: json['total_pages'],
      total_results: json['total_results'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['page'] = this.page;
    data['total_pages'] = this.total_pages;
    data['total_results'] = this.total_results;
    if (this.results != null) {
      data['results'] = this.results.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

