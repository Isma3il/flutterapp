import 'package:domain/entities/review.dart';
class ReviewModel extends Review {
  final String author;
  final String content;
  final String id;
  final String url;

  ReviewModel({this.author, this.content, this.id, this.url})
  :super(author:author,url:url,id:id,content:content);

  factory ReviewModel.fromJson(Map<String, dynamic> json) {
    return ReviewModel(
      author: json['author'],
      content: json['content'],
      id: json['id'],
      url: json['url'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['author'] = this.author;
    data['content'] = this.content;
    data['id'] = this.id;
    data['url'] = this.url;
    return data;
  }
}