import 'dart:convert';
import 'movie_model.dart';

class MoviesResponse {
  final int page;
  final int total_pages;
  final int total_results;
  final List<MovieModel> results;

  MoviesResponse({this.page, this.total_pages, this.total_results, this.results});


  factory MoviesResponse.fromRawJson(String str) => MoviesResponse.fromJson(json.decode(str));

  factory MoviesResponse.fromJson(Map<String, dynamic> json) {
    return MoviesResponse(
        page: json['page'],
        total_pages: json['total_pages'],
        total_results: json['total_results'],
        results: List<MovieModel>.from(json["results"].map((x) => MovieModel.fromJson(x)))
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['page'] = this.page;
    data['total_pages'] = this.total_pages;
    data['total_results'] = this.total_results;
    data['results'] = List<dynamic>.from(results.map((e) => e.toJson()));
    return data;
  }

  @override
  String toString() =>"page $page total_pages $total_pages total_results $total_results results :  ${results.toString()}";
}