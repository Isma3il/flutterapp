import 'package:core/error/exception.dart';
import 'package:data/api/api_helper.dart';
import 'package:data/api/model/movie_response.dart';
import 'package:data/api/model/movie_reviews.dart';
import 'package:data/api/model/movie_videos.dart';
import 'package:data/config/config.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
class AppApiHelper implements ApiHelper{

  final Dio dio;

  AppApiHelper({@required this.dio});

  @override
  Future<MoviesResponse> getMostPopularMovie() async {
    try{
      Response response=await dio.get("popular?api_key=${Config.API_KEY}");
      return MoviesResponse.fromJson(response.data);
    }catch(e){
      throw ServerException();
    }
  }

  @override
  Future<MoviesResponse> getTopRatedMovie() async {
    try{
      Response response=await dio.get("top_rated?api_key=${Config.API_KEY}");
      return MoviesResponse.fromJson(response.data);
    }catch(e){
      throw ServerException();
    }
  }

  @override
  Future<MovieReviews> getMovieReviews(String movieId) async {
    try{
      Response response = await dio.get("$movieId/reviews?api_key=${Config.API_KEY}");
      return MovieReviews.fromJson(response.data);
    }catch(e){
      throw ServerException();
    }
  }

  @override
  Future<MovieVideos> getMovieVideos(String movieId) async{
    try{
      Response response = await dio.get("$movieId/videos?api_key=${Config.API_KEY}");
      return MovieVideos.fromJson(response.data);
    }catch(e){
      throw ServerException();
    }

  }

}