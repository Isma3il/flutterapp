class Config {


  /* API key*/

  static String API_KEY = "3f1cd7fa782a61778afe7e6ceb8d1765";
  static String BASE_URL = "https://api.themoviedb.org/3/movie/";
  static String BASE_POSTER_URL = "https://image.tmdb.org/t/p/w500/";
  static String YOUTUBE_THUMBMAIL = "http://img.youtube.com/vi/";




  /*Database*/
  static String tblName = "movie";
  static String colId = "id";
  static String colTitle = "title";
  static String colOverview = "overview";
  static String colReleaseData = "release_data";
  static String colVoteCount = "vote_count";
  static String colVoteAverage = "vote_average";
  static String colPosterPath = "poster_path";


/* Shared Pref Keys */



}
