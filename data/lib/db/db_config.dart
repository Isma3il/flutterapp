
import 'package:sqflite/sqflite.dart';

class DbConfig{


  /*Table info*/
  String tblName = "movie";
  String colId = "id";
  String colTitle = "title";
  String colOverview = "overview";
  String colReleaseData = "release_data";
  String colVoteCount = "vote_count";
  String colVoteAverage = "vote_average";
  String colPosterPath = "poster_path";


}