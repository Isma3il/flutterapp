import 'package:data/config/config.dart';
import 'package:data/db/db_helper.dart';
import 'package:data/db/model/movie_db.dart';
import 'package:sqflite/sqflite.dart';
import 'package:meta/meta.dart';
class AppDbHelper implements DbHelper {

   Future<Database> database;


  AppDbHelper({@required this.database});

  @override
  Future<int> deleteMovie(int id) async {
    Database db = await database;
    return db.delete(Config.tblName,where: '${Config.colId} = ?',whereArgs: [id]);
  }

  @override
  Future<MovieDb> getMovie(int id) async {
    Database db = await database;

    List<Map> maps = await db.query(
        Config.tblName,
        columns: [Config.colId],
        where: '${Config.colId} = ?',
        whereArgs: [id]);
    if (maps.length > 0) {
      return MovieDb.fromObject(maps.first);
    }
    return null;
  }

  @override
  Future<List<MovieDb>> getMovies() async{
    Database db = await database;

    var result= await db.rawQuery("SELECT * FROM ${Config.tblName}");
    List<MovieDb> movies =  List<MovieDb>();
    result.forEach((f) => movies.add(MovieDb.fromObject(f)));
    return movies;
  }

  @override
  Future<int> insertMovie(MovieDb movie) async {
    Database db = await database;
    return await db.insert(Config.tblName, movie.toMap());
  }
}
