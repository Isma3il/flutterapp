import 'package:data/db/model/movie_db.dart';

abstract class DbHelper{


  Future<int> insertMovie(MovieDb movie);

  Future<List<MovieDb>> getMovies();

  Future<MovieDb> getMovie(int id);

  Future<int> deleteMovie(int id);

}