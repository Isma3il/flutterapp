import 'package:data/pref/pref_helper.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:meta/meta.dart';
class AppPrefHelper implements PrefHelper{

  final FlutterSecureStorage secureStorage;
  AppPrefHelper({@required this.secureStorage});

}