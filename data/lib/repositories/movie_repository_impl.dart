import 'package:core/error/failure.dart';
import 'package:core/platform/network_info.dart';
import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';
import 'package:data/api/api_helper.dart';
import 'package:data/db/db_helper.dart';
import 'package:data/pref/pref_helper.dart';
import 'package:domain/entities/movie.dart';
import 'package:domain/repositories/movie_repository.dart';

class MovieRepositoryImpl implements MovieRepository {
  final ApiHelper apiHelper;
  final DbHelper dbHelper;
  final PrefHelper prefHelper;
  final NetworkInfo networkInfo;

  MovieRepositoryImpl(
      {@required this.apiHelper,
      this.dbHelper,
      this.prefHelper,
      this.networkInfo});

  @override
  Future<Either<Failure, List<Movie>>> getMostPopularMovie() async {
    if (await networkInfo.isConnected) {
      try {
        final response = await apiHelper.getMostPopularMovie();

        return Right(response.results);
      } catch (e) {
        return Left(ServerFailure());
      }
    } else {
      return Left(ServerFailure());
    }
  }

  @override
  Future<Either<Failure, List<Movie>>> getTopRatedMovie() async {
    if (await networkInfo.isConnected) {
      try {
        final response = await apiHelper.getTopRatedMovie();
        return Right(response.results);
      } catch (e) {
        return Left(ServerFailure());
      }
    } else {
      return Left(ServerFailure());
    }
  }
}
