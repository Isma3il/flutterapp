import 'package:core/error/failure.dart';
import 'package:core/platform/network_info.dart';
import 'package:dartz/dartz.dart';
import 'package:data/api/api_helper.dart';
import 'package:data/data.dart';
import 'package:data/db/model/movie_db.dart';
import 'package:domain/entities/movie.dart';
import 'package:domain/entities/review.dart';
import 'package:domain/entities/video.dart';
import 'package:domain/repositories/movie_detail_repository.dart';

class MovieDetailsRepositoryImpl implements MovieDetailRepository {
  final ApiHelper apiHelper;
  final DbHelper dbHelper;
  final NetworkInfo networkInfo;

  MovieDetailsRepositoryImpl({this.apiHelper, this.networkInfo, this.dbHelper});

  @override
  Future<Either<Failure, List<Review>>> getMovieReviews(String movieId) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await apiHelper.getMovieReviews(movieId);
        return Right(response.results);
      } catch (e) {
        return Left(ServerFailure());
      }
    } else {
      return Left(ServerFailure());
    }
  }

  @override
  Future<Either<Failure, List<Video>>> getMovieVideos(String movieId) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await apiHelper.getMovieVideos(movieId);
        return Right(response.results);
      } catch (e) {
        return Left(ServerFailure());
      }
    } else {
      return Left(ServerFailure());
    }
  }

  @override
  Future<Either<Failure, Movie>> getMovie(int id) async {
    final movie = await dbHelper.getMovie(id);
    if (movie != null) {
      return Right(await dbHelper.getMovie(id));
    } else {
      return Left(CacheFailure());
    }
  }

  @override
  Future<Either<Failure, int>> deleteMovie(int id) async {
    final deletedMovie = await dbHelper.deleteMovie(id);
    if (deletedMovie != -1) {
      return Right(deletedMovie);
    } else {
      return Left(CacheFailure());
    }
  }

  @override
  Future<Either<Failure, int>> insertMovie(Movie movie) async {
    final MovieDb movieDb = MovieDb(movie.id, movie.title, movie.description, movie.releaseData, movie.voteCount, movie.voteAverage, movie.poster);
    final insertedMovie = await dbHelper.insertMovie(movieDb);
    if (insertedMovie != -1) {
      return Right(insertedMovie);
    } else {
      return Left(CacheFailure());
    }
  }
}
