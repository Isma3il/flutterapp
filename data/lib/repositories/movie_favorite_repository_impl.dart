import 'package:core/error/failure.dart';
import 'package:dartz/dartz.dart';
import 'package:data/db/db_helper.dart';
import 'package:domain/entities/movie.dart';
import 'package:domain/repositories/movie_favorite_repository.dart';

class MovieFavoriteRepositoryImpl implements MovieFavoriteRepository {
  final DbHelper dbHelper;

  MovieFavoriteRepositoryImpl({this.dbHelper});

  @override
  Future<Either<Failure, int>> deleteMovie(int id) async {
    final deletedMovie = await dbHelper.deleteMovie(id);
    if (deletedMovie != -1) {
      return Right(deletedMovie);
    } else {
      return Left(CacheFailure());
    }
  }

  @override
  Future<Either<Failure, List<Movie>>> getMovies() async {
    final movies = await dbHelper.getMovies();
    if (movies != null) {
      return Right(movies);
    } else {
      return Left(CacheFailure());
    }
  }
}
